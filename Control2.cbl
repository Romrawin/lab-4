       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL2.
       AUTHOR. ROMRAWIN.

       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES. 
           CLASS HEX-NUMBER IS "0" THRU "9", "A" THRU "F"
           CLASS REAL-NAME IS "A" THRU "Z", "a" THRU "z","'", SPACE.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM-IN PIC X(4).
       01  NAME-IN PIC X(15).

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Enter a Hex number - " WITH NO ADVANCING
           ACCEPT NUM-IN
           IF NUM-IN IS HEX-NUMBER THEN
              DISPLAY NUM-IN " is a Hex number"
           ELSE
              DISPLAY NUM-IN " is not a Hex number"
           END-IF

           DISPLAY "Enter a Real name - " WITH NO ADVANCING
           ACCEPT NUM-IN
           IF NAME-IN  IS REAL-NAME THEN
              DISPLAY NAME-IN  " is a real name"
           ELSE
              DISPLAY NUM-IN " is not a real name"
           END-IF

           DISPLAY "Enter a name - " WITH NO ADVANCING
           ACCEPT NUM-IN
           IF NAME-IN  IS ALPHABETIC  THEN
              DISPLAY NAME-IN  " is a name"
           ELSE
              DISPLAY NUM-IN " is not a name"
           END-IF
           .