       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. ROMRAWIN.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  HEIGHTT PIC 999V9(2) VALUE ZERO .
       01  WEIGHT PIC 999V9(2) VALUE ZERO .
       01  BMI PIC 99V9(1) VALUE ZERO .
       01  HUN   PIC   999 VALUE 100.

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Enter Height (cm): " WITH NO ADVANCING 
           ACCEPT HEIGHTT 
           DISPLAY "Enter Weight (kg): " WITH NO ADVANCING 
           ACCEPT WEIGHT
 
           DIVIDE HEIGHTT   BY  HUN    GIVING HEIGHTT 
           MULTIPLY HEIGHTT  BY HEIGHTT
           DIVIDE WEIGHT  BY  HEIGHTT   GIVING BMI 

           DISPLAY "Your bmi  is : " BMI

           IF BMI < 18.50 THEN
              DISPLAY "Underweight"
           END-IF 
           IF BMI >= 18.5 AND BMI <= 24.9 THEN
              DISPLAY "Normal weight"
           END-IF
           IF BMI >= 25 AND BMI <= 29.9 THEN
              DISPLAY "Overweight "
           END-IF 
           IF BMI >= 30 THEN
              DISPLAY "Obesity"
           END-IF 
           .
